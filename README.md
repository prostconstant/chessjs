Jeu réalisé en français dans le but d'apprendre JavaScript et d'être repris plus facilement par un débutant français.

Ce jeu d'echec est composé de 3 éléments :

    - index.html
    - style.css
    - script.js


Le fichier index.html:

    Ce fichier est le fichier contenant les bases de l'application.
    C'est ce fichier que l'on va appeller lorsque l'on rentre l'URL http://localhost:8080/echecJs/
    C'est le fichier central qui lie les 2 autres entre ses balises :
        <head>
            <link rel="stylesheet" href="css\style.css"> //Fait référence au fichier css
            <script src="js\script.js" defer></script> //Fait référence au fichier javascript
        </head>

    Ce fichier utilise des balises pour permettre l'affichage de tous les éléments de la page.
    Les éléments commencent avec une balise ouvrante du style : "<div>"
    et se terminent par une balise fermante du type : "</div>"

    Dans ces éléments, il peut y avoir des arguments tels que les "class" qui permettent au fichier css
    de venir modifier l'apparence des éléments concernés selon le type de "class" entré.

    Ces éléments comportent aussi des paramètres "id" qui vont permettre à chaque élément d'être modifié
    par le fichier javascript (.js).

    Certains éléments font aussi références à des images et comportent alors le paramètre "src" qui va
    cibler une image dans le dossier "pieces".


Le fichier css :

    Ce fichier va gérer toutes les modifications d'affichage du fichier idex.html selon les classes des
    différents objets.

    Pour se rendre compte du travail qu'il fait, on peut renommer temporairement le fichier pour que le 
    fichier html ne puisse plus le localiser.

    Le fichier css est organisé de la manière suivante :

        On peut retrouver le nom des classes précédées d'un point auxquelles on va apporter des 
        modifications en début de ligne. Ex: .infoJoueur { ... } -> les paramètres entre crochets seront 
        appliqués aux éléments portants le nom de classe "infoJoueur"
        Chaque nom de classe contient un ensemble de paramètres qui sont tous contenus entre les crochets "{}"
        Les noms d'éléments ne commençant pas par un point concernent tous les éléments contenu entre les
        balises ciblées : Ex: body{ ... }


Le fichier Javascript (js) :

    Ce fichier va permettre de gérer l'intégralité de l'application sans avoir à recharger la page.
    Le fichier a été entièrement commenté afin de permettre sa compréhension.

    Ce fichier va pouvoir accèder, comme indiqué précédemment à tous les élément du fichier html qui comportent
    un paramètre "id".

    le langage javascript a plusieurs spécificités :

        - Une ligne se termine TOUJOURS par un point virgule (;) -> Pas de point virgule = erreur
        - Au sein de ce fichier, on va pouvoir déclarer des variables de la manière suivante :
            var variable = 0;
        - On peut faire des commentaires sur la feuille à l'aide des "//" -> Tout ce qui se trouve après cela
        sera un commentaire
        - En javascript, on peut modifier intégralement le fichier. On peut manier le contenu en en ajoutant
        ou modifiant son apparence en modifiant les classes des différents éléments afin que celles-ci 
        correpondent à des éléments CSS différents. Ex: Modifier la classe "black" en "white" pour que la case
        s'affiche en blanc au lieu de s'afficher en noir.


Dans cette application, on part d'un fichier HTML auquel on lie un fichier Javascript et un fichier CSS qui 
vont permettre au contenu du fichier HTML d'être dynamique. Tous les calculs se font directement sur la
machine de l'utilisateur.


Installation :
Pour accèder au projet au mieux :

    Installer Laragon : https://laragon.org/download/

    Installer Visual Studio code : https://code.visualstudio.com/download

Une fois cela fait, ouvrir les 2 applications, lancer laragon (bouton "Start all" en bas à gauche)
Ouvrir Visual Studio code -> Fichier > Ouvrir le dossier, selectionner le dossier C:\laragon\www

Ouvrir l'application en cours de création -> sur laragon, Clic droit > www > echecJs
L'URL devrait avoir ce format http://echecjs.test:8080/ ou alors http://localhost:8080/echecJs/

Pour faciliter le traitement des fichiers dans Visual Studio Code -> 
Dans le menu à gauche ->  Extensions
Installer les Extensions -> 
    - Visual Studio IntelliCode
    - JS-CSS-HTML Formatter
    - IntelliSense for CSS class names in HTML
    - HTML Snippets
    - CSS Formatter

Vous êtes maintenant prêt à reprendre ce projet, bonne chance !

