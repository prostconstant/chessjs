//Les différentes classes (objets) utilisées dans l'application
class Case { //Une case = un bouton, une pièce -> qui peut être null, une valeur X, une valeur Y
    constructor(btn, piece, repX, repY) {
        this.btn = btn;
        this.piece = piece;
        this.repX = repX;
        this.repY = repY;
    }
}

class Piece { //Une pièce = un type (P = pion, R = roi, ...), une couleur (blanc ou noir)
    constructor(type, color) {
        this.type = type;
        this.color = color;
    }
}

class Position { // Une Position = Une valeur X (Abscisse), une valeur Y (Ordonnée)
    constructor(X, Y) {
        this.X = X;
        this.Y = Y;
    }
}

class CaseModif { //Une case modifiée = un bouton (element html -> case de l'echiquier), une couleur de base 
    constructor(btn, color) {
        this.btn = btn;
        this.color = color;
    }
}
//

//Récupération de tous les objets utiles au JS -> Get by id
//GET tous les boutons (cases) qui forment l'échiquier
const A1 = document.getElementById("A1");
const A2 = document.getElementById("A2");
const A3 = document.getElementById("A3");
const A4 = document.getElementById("A4");
const A5 = document.getElementById("A5");
const A6 = document.getElementById("A6");
const A7 = document.getElementById("A7");
const A8 = document.getElementById("A8");
const B1 = document.getElementById("B1");
const B2 = document.getElementById("B2");
const B3 = document.getElementById("B3");
const B4 = document.getElementById("B4");
const B5 = document.getElementById("B5");
const B6 = document.getElementById("B6");
const B7 = document.getElementById("B7");
const B8 = document.getElementById("B8");
const C1 = document.getElementById("C1");
const C2 = document.getElementById("C2");
const C3 = document.getElementById("C3");
const C4 = document.getElementById("C4");
const C5 = document.getElementById("C5");
const C6 = document.getElementById("C6");
const C7 = document.getElementById("C7");
const C8 = document.getElementById("C8");
const D1 = document.getElementById("D1");
const D2 = document.getElementById("D2");
const D3 = document.getElementById("D3");
const D4 = document.getElementById("D4");
const D5 = document.getElementById("D5");
const D6 = document.getElementById("D6");
const D7 = document.getElementById("D7");
const D8 = document.getElementById("D8");
const E1 = document.getElementById("E1");
const E2 = document.getElementById("E2");
const E3 = document.getElementById("E3");
const E4 = document.getElementById("E4");
const E5 = document.getElementById("E5");
const E6 = document.getElementById("E6");
const E7 = document.getElementById("E7");
const E8 = document.getElementById("E8");
const F1 = document.getElementById("F1");
const F2 = document.getElementById("F2");
const F3 = document.getElementById("F3");
const F4 = document.getElementById("F4");
const F5 = document.getElementById("F5");
const F6 = document.getElementById("F6");
const F7 = document.getElementById("F7");
const F8 = document.getElementById("F8");
const G1 = document.getElementById("G1");
const G2 = document.getElementById("G2");
const G3 = document.getElementById("G3");
const G4 = document.getElementById("G4");
const G5 = document.getElementById("G5");
const G6 = document.getElementById("G6");
const G7 = document.getElementById("G7");
const G8 = document.getElementById("G8");
const H1 = document.getElementById("H1");
const H2 = document.getElementById("H2");
const H3 = document.getElementById("H3");
const H4 = document.getElementById("H4");
const H5 = document.getElementById("H5");
const H6 = document.getElementById("H6");
const H7 = document.getElementById("H7");
const H8 = document.getElementById("H8");
//

//Autres éléments
const affTour = document.getElementById("affTour");
const btnRestart = document.getElementById("btnRestart");
const btnVictoWhite = document.getElementById("btnVictoWhite");
const btnVictoBlack = document.getElementById("btnVictoBlack");
const turn = document.getElementById("turn");
const msgBox = document.getElementById("msg");
const affScores = document.getElementById("affScores");
const listPrise = document.getElementById("listPrise");
//

//Définition des fonctions associées à chaque bouton, un clic lance la fonction associée
btnRestart.addEventListener("click", () => { restart() });
A1.addEventListener("click", () => { pressed("A", 1) });
A2.addEventListener("click", () => { pressed("A", 2) });
A3.addEventListener("click", () => { pressed("A", 3) });
A4.addEventListener("click", () => { pressed("A", 4) });
A5.addEventListener("click", () => { pressed("A", 5) });
A6.addEventListener("click", () => { pressed("A", 6) });
A7.addEventListener("click", () => { pressed("A", 7) });
A8.addEventListener("click", () => { pressed("A", 8) });
B1.addEventListener("click", () => { pressed("B", 1) });
B2.addEventListener("click", () => { pressed("B", 2) });
B3.addEventListener("click", () => { pressed("B", 3) });
B4.addEventListener("click", () => { pressed("B", 4) });
B5.addEventListener("click", () => { pressed("B", 5) });
B6.addEventListener("click", () => { pressed("B", 6) });
B7.addEventListener("click", () => { pressed("B", 7) });
B8.addEventListener("click", () => { pressed("B", 8) });
C1.addEventListener("click", () => { pressed("C", 1) });
C2.addEventListener("click", () => { pressed("C", 2) });
C3.addEventListener("click", () => { pressed("C", 3) });
C4.addEventListener("click", () => { pressed("C", 4) });
C5.addEventListener("click", () => { pressed("C", 5) });
C6.addEventListener("click", () => { pressed("C", 6) });
C7.addEventListener("click", () => { pressed("C", 7) });
C8.addEventListener("click", () => { pressed("C", 8) });
D1.addEventListener("click", () => { pressed("D", 1) });
D2.addEventListener("click", () => { pressed("D", 2) });
D3.addEventListener("click", () => { pressed("D", 3) });
D4.addEventListener("click", () => { pressed("D", 4) });
D5.addEventListener("click", () => { pressed("D", 5) });
D6.addEventListener("click", () => { pressed("D", 6) });
D7.addEventListener("click", () => { pressed("D", 7) });
D8.addEventListener("click", () => { pressed("D", 8) });
E1.addEventListener("click", () => { pressed("E", 1) });
E2.addEventListener("click", () => { pressed("E", 2) });
E3.addEventListener("click", () => { pressed("E", 3) });
E4.addEventListener("click", () => { pressed("E", 4) });
E5.addEventListener("click", () => { pressed("E", 5) });
E6.addEventListener("click", () => { pressed("E", 6) });
E7.addEventListener("click", () => { pressed("E", 7) });
E8.addEventListener("click", () => { pressed("E", 8) });
F1.addEventListener("click", () => { pressed("F", 1) });
F2.addEventListener("click", () => { pressed("F", 2) });
F3.addEventListener("click", () => { pressed("F", 3) });
F4.addEventListener("click", () => { pressed("F", 4) });
F5.addEventListener("click", () => { pressed("F", 5) });
F6.addEventListener("click", () => { pressed("F", 6) });
F7.addEventListener("click", () => { pressed("F", 7) });
F8.addEventListener("click", () => { pressed("F", 8) });
G1.addEventListener("click", () => { pressed("G", 1) });
G2.addEventListener("click", () => { pressed("G", 2) });
G3.addEventListener("click", () => { pressed("G", 3) });
G4.addEventListener("click", () => { pressed("G", 4) });
G5.addEventListener("click", () => { pressed("G", 5) });
G6.addEventListener("click", () => { pressed("G", 6) });
G7.addEventListener("click", () => { pressed("G", 7) });
G8.addEventListener("click", () => { pressed("G", 8) });
H1.addEventListener("click", () => { pressed("H", 1) });
H2.addEventListener("click", () => { pressed("H", 2) });
H3.addEventListener("click", () => { pressed("H", 3) });
H4.addEventListener("click", () => { pressed("H", 4) });
H5.addEventListener("click", () => { pressed("H", 5) });
H6.addEventListener("click", () => { pressed("H", 6) });
H7.addEventListener("click", () => { pressed("H", 7) });
H8.addEventListener("click", () => { pressed("H", 8) });
btnVictoBlack.addEventListener("click", () => { ajouterScore("black") });
btnVictoWhite.addEventListener("click", () => { ajouterScore("white") });
//

//Varibales globales de l'application
//Initialisation des listes
var listPossibilite = [];
var listCaseModif = [];
var listCase = [
    new Case(A1, null, 0, 0),
    new Case(A2, null, 0, 1),
    new Case(A3, null, 0, 2),
    new Case(A4, null, 0, 3),
    new Case(A5, null, 0, 4),
    new Case(A6, null, 0, 5),
    new Case(A7, null, 0, 6),
    new Case(A8, null, 0, 7),
    new Case(B1, null, 1, 0),
    new Case(B2, null, 1, 1),
    new Case(B3, null, 1, 2),
    new Case(B4, null, 1, 3),
    new Case(B5, null, 1, 4),
    new Case(B6, null, 1, 5),
    new Case(B7, null, 1, 6),
    new Case(B8, null, 1, 7),
    new Case(C1, null, 2, 0),
    new Case(C2, null, 2, 1),
    new Case(C3, null, 2, 2),
    new Case(C4, null, 2, 3),
    new Case(C5, null, 2, 4),
    new Case(C6, null, 2, 5),
    new Case(C7, null, 2, 6),
    new Case(C8, null, 2, 7),
    new Case(D1, null, 3, 0),
    new Case(D2, null, 3, 1),
    new Case(D3, null, 3, 2),
    new Case(D4, null, 3, 3),
    new Case(D5, null, 3, 4),
    new Case(D6, null, 3, 5),
    new Case(D7, null, 3, 6),
    new Case(D8, null, 3, 7),
    new Case(E1, null, 4, 0),
    new Case(E2, null, 4, 1),
    new Case(E3, null, 4, 2),
    new Case(E4, null, 4, 3),
    new Case(E5, null, 4, 4),
    new Case(E6, null, 4, 5),
    new Case(E7, null, 4, 6),
    new Case(E8, null, 4, 7),
    new Case(F1, null, 5, 0),
    new Case(F2, null, 5, 1),
    new Case(F3, null, 5, 2),
    new Case(F4, null, 5, 3),
    new Case(F5, null, 5, 4),
    new Case(F6, null, 5, 5),
    new Case(F7, null, 5, 6),
    new Case(F8, null, 5, 7),
    new Case(G1, null, 6, 0),
    new Case(G2, null, 6, 1),
    new Case(G3, null, 6, 2),
    new Case(G4, null, 6, 3),
    new Case(G5, null, 6, 4),
    new Case(G6, null, 6, 5),
    new Case(G7, null, 6, 6),
    new Case(G8, null, 6, 7),
    new Case(H1, null, 7, 0),
    new Case(H2, null, 7, 1),
    new Case(H3, null, 7, 2),
    new Case(H4, null, 7, 3),
    new Case(H5, null, 7, 4),
    new Case(H6, null, 7, 5),
    new Case(H7, null, 7, 6),
    new Case(H8, null, 7, 7)
];
var listPriseVal = [];
//

//Variables lambda
var playersTurn = 0; //0 -> Blanc, 1 -> Noir
var scoreBlack = 0;
var scoreWhite = 0;
var estTermine = false;
var premierClick = true;
var positionBase = null;
var positionArrivee = null;
var caseBase = null;
var caseCible = null;
var deplacementDone = false;
var echecRoiNoir = false;
var echecRoiBlanc = false;
var victoireBlanc = false;
var victoireNoir = false;
//

function restart() { //Affichage reset
    playersTurn = 0; //Tour des blancs
    affTour.innerHTML = "C'est aux blancs de jouer"; //Le texte dans l'éléments affTour (fichier html) change de valeur
    turn.className = turn.className.replace("blackTurn", "white"); //Passe turn sur le blanc
    listPriseVal = [];
    nouvellePartie(); //Fonction réinitialisant la partie
}

function onLoad() { //Actions au chargement
    affTour.innerHTML = "C'est aux blancs de jouer";
    nouvellePartie();
    afficherScores();

}

function pressed(lettre, nbr) { //Dans le cas où l'utilisateur clique sur une case du plateau

    console.log(lettre + nbr); //Affiche la poition de la case selctionnée dans la console (F12)

    //On parcourt la liste des cases pour vérifier que la case comporte une pièce
    listCase.forEach(element => {
        if (element.btn.id == lettre + nbr) {
            if (premierClick) { //Si c'est on a pas encore sélectionné la pièce à déplacer
                if (element.piece != null) { //Si la case comporte une pièce
                    if (playersTurn == 0 && element.piece.color == "Blanc" || playersTurn == 1 && element.piece.color == "Noir") { //Si c'est bien le tour du joueur et c'est une pièce de sa couleur
                        positionBase = new Position(element.repX, element.repY); //Enregistrement de la position de la pièce selectionnée
                        proposerMouv(element.piece.type, element.piece.color, element.repX, element.repY); //Fait la liste des Mouvement possibles
                        caseBase = element; //Enregistrement de la case "source"
                        premierClick = false; //le premier click a été fait et le prochain sera le déplcement de la pièce
                    } else { //Si ce n'est pas la bonne couleur
                        afficherMsg("Ce n'est pas votre tour");
                    }
                } else { //Si la case ne comporte pas de pièce
                    afficherMsg("Cette case est vide");
                }
            } else { //Si c'est le second click
                deplacerPion(new Position(element.repX, element.repY));
                premierClick = true;

                if (deplacementDone) {
                    if (playersTurn == 0) {
                        playersTurn = 1;
                        affTour.innerHTML = "C'est aux noirs de jouer";
                        turn.className = turn.className.replace("white", "blackTurn");
                    } else {
                        playersTurn = 0;
                        affTour.innerHTML = "C'est aux blancs de jouer";
                        turn.className = turn.className.replace("blackTurn", "white");
                    }
                    deplacementDone = false;

                    checkGameOver();
                }

                checkIfEchec();
            }
        }
    });
}

function nouvellePartie() { //Reset la partie
    clearPieces(); //Efface toutes les pièces de l'échiquier
    addAllPiece(); //Ajoute toutes les pièces avec leur position initiale
    refreshTab(); //Recharge l'apparence de l'échiquier
    afficherListePrise();
}

function refreshTab() { //Rafraichis l'affichage du plateau

    //Parcourt la liste des cases pour afficher les différentes images dans les cases correspondantes
    listCase.forEach(element => {
        var image = document.getElementById("img" + element.btn.id);
        if (element.piece != null) { //Si la case comporte une pièce 
            image.src = "pieces\\" + element.piece.type + element.piece.color + ".gif"; //Importe les images
        } else { //Si la case ne comporte pas de pièce
            if (element.color == "white") { //Si la case est blanche
                image.src = "pieces\\voidBlanc.gif";
            } else { //Si la case est noire
                image.src = "pieces\\voidNoir.gif";
            }

        }
    });
}

function addAllPiece() { //Ajoute les pièces pour commencer une partie

    //On définit, pour chaque case concernée, la pièce de base qui est concernée
    listCase[0].piece = new Piece("T", "Blanc");
    listCase[1].piece = new Piece("P", "Blanc");
    listCase[6].piece = new Piece("P", "Noir");
    listCase[7].piece = new Piece("T", "Noir");

    listCase[8].piece = new Piece("C", "Blanc");
    listCase[9].piece = new Piece("P", "Blanc");
    listCase[14].piece = new Piece("P", "Noir");
    listCase[15].piece = new Piece("C", "Noir");

    listCase[16].piece = new Piece("F", "Blanc");
    listCase[17].piece = new Piece("P", "Blanc");
    listCase[22].piece = new Piece("P", "Noir");
    listCase[23].piece = new Piece("F", "Noir");

    listCase[24].piece = new Piece("D", "Blanc");
    listCase[25].piece = new Piece("P", "Blanc");
    listCase[30].piece = new Piece("P", "Noir");
    listCase[31].piece = new Piece("D", "Noir");

    listCase[32].piece = new Piece("R", "Blanc");
    listCase[33].piece = new Piece("P", "Blanc");
    listCase[38].piece = new Piece("P", "Noir");
    listCase[39].piece = new Piece("R", "Noir");

    listCase[40].piece = new Piece("F", "Blanc");
    listCase[41].piece = new Piece("P", "Blanc");
    listCase[46].piece = new Piece("P", "Noir");
    listCase[47].piece = new Piece("F", "Noir");

    listCase[48].piece = new Piece("C", "Blanc");
    listCase[49].piece = new Piece("P", "Blanc");
    listCase[54].piece = new Piece("P", "Noir");
    listCase[55].piece = new Piece("C", "Noir");

    listCase[56].piece = new Piece("T", "Blanc");
    listCase[57].piece = new Piece("P", "Blanc");
    listCase[62].piece = new Piece("P", "Noir");
    listCase[63].piece = new Piece("T", "Noir");
    //
}

function proposerMouv(type, color, X, Y) { //Renvoie sur la bonne fonction -> Proposition mouvement en fcn du type de la pièce
    //Réinitialise
    listPossibilite = []; //Vide la liste des movements possibles
    echecRoiBlanc = false;
    echecRoiNoir = false;
    //

    //Chercher la liste des possibilités de déplacement
    switch (type) {
        //Déplacement du pion 
        case "P":
            montrerP(color, X, Y);
            break;

            //Déplacement du fou
        case "F":
            montrerF(color, X, Y);
            break;

            //Déplacement de la dame
        case "D":
            montrerD(color, X, Y);
            break;

            //Déplacement du roi
        case "R":
            montrerR(color, X, Y);
            break;

            //Déplacement du cavalier
        case "C":
            montrerC(color, X, Y);
            break;

            //Déplacement de la tour
        case "T":
            montrerT(color, X, Y);
            break;
    }
    //

    //Afficher la liste des possibilités de déplacement
    affListPossib();
    //
}

function montrerC(color, X, Y) { //Définis les possibilités de mouvement du cavalier

    listCase.forEach(element => { //Pour chaque case, on fait les actions suivantes

        //Vérification de la position X+1 et Y+2
        if (element.repX == X + 1 && element.repY == Y + 2) {
            if (element.piece != null) { //Si la case comporte déjà une pièce
                if (element.piece.color != color) { //Si la pièce est une pièce ennemi
                    checkIfKing(new Position(X + 1, Y + 2), color);
                    listPossibilite.push(new Position(X + 1, Y + 2));
                }
            } else { //Si la case ne comporte pas déjà une pièce, ajout de la case aux possibilités
                listPossibilite.push(new Position(X + 1, Y + 2));
            }
        }
        //Fonctionnement identique pour les vérifications suivantes

        //Vérification de la position X+2 et Y+1
        if (element.repX == X + 2 && element.repY == Y + 1) {
            if (element.piece != null) {
                if (element.piece.color != color) {
                    checkIfKing(new Position(X + 2, Y + 1), color);
                    listPossibilite.push(new Position(X + 2, Y + 1));
                }
            } else {
                listPossibilite.push(new Position(X + 2, Y + 1));
            }
        }

        //Vérification de la position X-1 et Y-2
        if (element.repX == X - 1 && element.repY == Y - 2) {
            if (element.piece != null) {
                if (element.piece.color != color) {
                    checkIfKing(new Position(X - 1, Y - 2), color);
                    listPossibilite.push(new Position(X - 1, Y - 2));
                }
            } else {
                listPossibilite.push(new Position(X - 1, Y - 2));
            }
        }

        //Vérification de la position X+1 et Y-2
        if (element.repX == X + 1 && element.repY == Y - 2) {
            if (element.piece != null) {
                if (element.piece.color != color) {
                    checkIfKing(new Position(X + 1, Y - 2), color);
                    listPossibilite.push(new Position(X + 1, Y - 2));
                }
            } else {
                listPossibilite.push(new Position(X + 1, Y - 2));
            }
        }

        //Vérification de la position X+2 et Y+1
        if (element.repX == X + 2 && element.repY == Y - 1) {
            if (element.piece != null) {
                if (element.piece.color != color) {
                    checkIfKing(new Position(X + 2, Y - 1), color);
                    listPossibilite.push(new Position(X + 2, Y - 1));
                }
            } else {
                listPossibilite.push(new Position(X + 2, Y - 1));
            }
        }

        //Vérification de la position X-2 et Y-1
        if (element.repX == X - 2 && element.repY == Y - 1) {
            if (element.piece != null) {
                if (element.piece.color != color) {
                    checkIfKing(new Position(X - 2, Y - 1), color);
                    listPossibilite.push(new Position(X - 2, Y - 1));
                }
            } else {
                listPossibilite.push(new Position(X - 2, Y - 1));
            }
        }

        //Vérification de la position X-2 et Y+1
        if (element.repX == X - 2 && element.repY == Y + 1) {
            if (element.piece != null) {
                if (element.piece.color != color) {
                    checkIfKing(new Position(X - 2, Y + 1), color);
                    listPossibilite.push(new Position(X - 2, Y + 1));
                }
            } else {
                listPossibilite.push(new Position(X - 2, Y + 1));
            }
        }

        //Vérification de la position X-1 et Y+2
        if (element.repX == X - 1 && element.repY == Y + 2) {
            if (element.piece != null) {
                if (element.piece.color != color) {
                    checkIfKing(new Position(X - 1, Y + 2), color);
                    listPossibilite.push(new Position(X - 1, Y + 2));
                }
            } else {
                listPossibilite.push(new Position(X - 1, Y + 2));
            }
        }
    });

    //Toutes les possibilités ont étés traitées et ajoutées à la liste "listPossibilite"
}

function montrerD(color, X, Y) { //Définis les possibilités de mouvement de la dame

    var bloqued = false;

    var baseX = X;
    var baseY = Y;
    while (baseX > 0 && baseY > 0 && !bloqued) {
        baseX--;
        baseY--;
        listCase.forEach(element => {
            if (element.repX == baseX && element.repY == baseY) {
                if (element.piece == null) {
                    listPossibilite.push(new Position(baseX, baseY));
                } else {
                    if (element.piece.color != color) {
                        listPossibilite.push(new Position(baseX, baseY));
                    }
                    bloqued = true;
                }
            }
        });
    }

    bloqued = false;
    baseX = X;
    baseY = Y;
    while (baseX < 8 && baseY < 8 && !bloqued) {
        baseX++;
        baseY++;
        listCase.forEach(element => {
            if (element.repX == baseX && element.repY == baseY) {
                if (element.piece == null) {
                    listPossibilite.push(new Position(baseX, baseY));
                } else {
                    if (element.piece.color != color) {
                        listPossibilite.push(new Position(baseX, baseY));
                    }
                    bloqued = true;
                }
            }
        });
    }

    bloqued = false;
    baseX = X;
    baseY = Y;
    while (baseX < 8 && baseY > 0 && !bloqued) {
        baseX++;
        baseY--;
        listCase.forEach(element => {
            if (element.repX == baseX && element.repY == baseY) {
                if (element.piece == null) {
                    listPossibilite.push(new Position(baseX, baseY));
                } else {
                    if (element.piece.color != color) {
                        listPossibilite.push(new Position(baseX, baseY));
                    }
                    bloqued = true;
                }
            }
        });
    }

    bloqued = false;
    baseX = X;
    baseY = Y;
    while (baseX > 0 && baseY < 8 && !bloqued) {
        baseX--;
        baseY++;
        listCase.forEach(element => {
            if (element.repX == baseX && element.repY == baseY) {
                if (element.piece == null) {
                    listPossibilite.push(new Position(baseX, baseY));
                } else {
                    if (element.piece.color != color) {
                        listPossibilite.push(new Position(baseX, baseY));
                    }
                    bloqued = true;
                }
            }
        });
    }

    //
    bloqued = false;

    var baseX = X;
    while (baseX > 0 && !bloqued) {
        baseX--;
        listCase.forEach(element => {
            if (element.repX == baseX && element.repY == Y) {
                if (element.piece == null) {
                    listPossibilite.push(new Position(baseX, Y));
                } else {
                    if (element.piece.color != color) {
                        listPossibilite.push(new Position(baseX, Y));
                    }
                    bloqued = true;
                }
            }
        });
    }

    bloqued = false;
    baseX = X;
    while (baseX < 8 && !bloqued) {
        baseX++;
        listCase.forEach(element => {
            if (element.repX == baseX && element.repY == Y) {
                if (element.piece == null) {
                    listPossibilite.push(new Position(baseX, Y));
                } else {
                    if (element.piece.color != color) {
                        listPossibilite.push(new Position(baseX, Y));
                    }
                    bloqued = true;
                }
            }
        });
    }

    bloqued = false;
    var baseY = Y;
    while (baseY >= 0 && !bloqued) {
        baseY--;
        listCase.forEach(element => {
            if (element.repX == X && element.repY == baseY) {
                if (element.piece == null) {
                    listPossibilite.push(new Position(X, baseY));
                } else {
                    if (element.piece.color != color) {
                        listPossibilite.push(new Position(X, baseY));
                    }
                    bloqued = true;
                }
            }
        });
    }

    bloqued = false;
    baseY = Y;
    while (baseY < 8 && !bloqued) {
        baseY++;
        listCase.forEach(element => {
            if (element.repX == X && element.repY == baseY) {
                if (element.piece == null) {
                    listPossibilite.push(new Position(X, baseY));
                } else {
                    if (element.piece.color != color) {
                        listPossibilite.push(new Position(X, baseY));
                    }
                    bloqued = true;
                }
            }
        });
    }

}

function montrerR(color, X, Y) { //Définis les possibilités de mouvement du roi

    listCase.forEach(element => {

        if (element.repX == X + 1 && element.repY == Y + 1) {
            if (element.piece != null) {
                if (element.piece.color != color) {
                    listPossibilite.push(new Position(X + 1, Y + 1));
                }
            } else {
                listPossibilite.push(new Position(X + 1, Y + 1));
            }
        }
        if (element.repX == X && element.repY == Y + 1) {
            if (element.piece != null) {
                if (element.piece.color != color) {
                    listPossibilite.push(new Position(X, Y + 1));
                }
            } else {
                listPossibilite.push(new Position(X, Y + 1));
            }
        }

        if (element.repX == X && element.repY == Y - 1) {
            if (element.piece != null) {
                if (element.piece.color != color) {
                    listPossibilite.push(new Position(X, Y - 1));
                }
            } else {
                listPossibilite.push(new Position(X, Y - 1));
            }
        }

        if (element.repX == X + 1 && element.repY == Y) {
            if (element.piece != null) {
                if (element.piece.color != color) {
                    listPossibilite.push(new Position(X + 1, Y));
                }
            } else {
                listPossibilite.push(new Position(X + 1, Y));
            }
        }

        if (element.repX == X + 1 && element.repY == Y - 1) {
            if (element.piece != null) {
                if (element.piece.color != color) {
                    listPossibilite.push(new Position(X + 1, Y - 1));
                }
            } else {
                listPossibilite.push(new Position(X + 1, Y - 1));
            }
        }

        if (element.repX == X - 1 && element.repY == Y - 1) {
            if (element.piece != null) {
                if (element.piece.color != color) {
                    listPossibilite.push(new Position(X - 1, Y - 1));
                }
            } else {
                listPossibilite.push(new Position(X - 1, Y - 1));
            }
        }

        if (element.repX == X - 1 && element.repY == Y) {
            if (element.piece != null) {
                if (element.piece.color != color) {
                    listPossibilite.push(new Position(X - 1, Y));
                }
            } else {
                listPossibilite.push(new Position(X - 1, Y));
            }
        }

        if (element.repX == X - 1 && element.repY == Y + 1) {
            if (element.piece != null) {
                if (element.piece.color != color) {
                    listPossibilite.push(new Position(X - 1, Y + 1));
                }
            } else {
                listPossibilite.push(new Position(X - 1, Y + 1));
            }
        }
    });
}

function montrerT(color, X, Y) { //Définis les possibilités de mouvement de la tour
    var bloqued = false;

    var baseX = X;
    while (baseX > 0 && !bloqued) {
        baseX--;
        listCase.forEach(element => {
            if (element.repX == baseX && element.repY == Y) {
                if (element.piece == null) {
                    listPossibilite.push(new Position(baseX, Y));
                } else {
                    if (element.piece.color != color) {
                        listPossibilite.push(new Position(baseX, Y));
                    }
                    bloqued = true;
                }
            }
        });
    }

    bloqued = false;
    baseX = X;
    while (baseX < 8 && !bloqued) {
        baseX++;
        listCase.forEach(element => {
            if (element.repX == baseX && element.repY == Y) {
                if (element.piece == null) {
                    listPossibilite.push(new Position(baseX, Y));
                } else {
                    if (element.piece.color != color) {
                        listPossibilite.push(new Position(baseX, Y));
                    }
                    bloqued = true;
                }
            }
        });
    }

    bloqued = false;
    var baseY = Y;
    while (baseY >= 0 && !bloqued) {
        baseY--;
        listCase.forEach(element => {
            if (element.repX == X && element.repY == baseY) {
                if (element.piece == null) {
                    listPossibilite.push(new Position(X, baseY));
                } else {
                    if (element.piece.color != color) {
                        listPossibilite.push(new Position(X, baseY));
                    }
                    bloqued = true;
                }
            }
        });
    }

    bloqued = false;
    baseY = Y;
    while (baseY < 8 && !bloqued) {
        baseY++;
        listCase.forEach(element => {
            if (element.repX == X && element.repY == baseY) {
                if (element.piece == null) {
                    listPossibilite.push(new Position(X, baseY));
                } else {
                    if (element.piece.color != color) {
                        listPossibilite.push(new Position(X, baseY));
                    }
                    bloqued = true;
                }
            }
        });
    }
}

function montrerP(color, X, Y) { //Définis les possibilités de mouvement du pion
    listCase.forEach(element => {

        if (color == "Noir") { //Avancer = Descendre --> Y - 1
            //Si un pièce en face
            if (element.repX == X && element.repY == Y - 1) {
                if (element.piece == null) {
                    listPossibilite.push(new Position(X, Y - 1));
                }
            }
            //

            //Si sur la ligne de spawn -> Peut avancer de 2 cases
            if (element.repX == X && element.repY == Y - 2) {
                var boolPieceDevant = false;

                listCase.forEach(elem => {
                    if (elem.repX == X && elem.repY == Y - 1) {
                        if (elem.piece != null) {
                            boolPieceDevant = true;
                        }
                    }
                });

                if (element.piece == null) {
                    if (Y == 6 && !boolPieceDevant) {
                        listPossibilite.push(new Position(X, Y - 2));
                    }
                }
            }
            //

            //Si prend une pièce
            if (element.repX == X + 1 && element.repY == Y - 1) {
                if (element.piece != null) {
                    if (element.piece.color != "Noir") {
                        listPossibilite.push(new Position(X + 1, Y - 1));
                    }
                }
            }

            if (element.repX == X - 1 && element.repY == Y - 1) {
                if (element.piece != null) {
                    if (element.piece.color != "Noir") {
                        listPossibilite.push(new Position(X - 1, Y - 1));
                    }
                }
            }
            //

        } else {
            //Si un pièce en face
            if (element.repX == X && element.repY == Y + 1) {
                if (element.piece == null) {
                    listPossibilite.push(new Position(X, Y + 1));
                }
            }
            //

            //Si sur la ligne de spawn -> Peut avancer de 2 cases
            if (element.repX == X && element.repY == Y + 2) {

                var boolPieceDevant = false;

                listCase.forEach(elem => {
                    if (elem.repX == X && elem.repY == Y + 1) {
                        if (elem.piece != null) {
                            boolPieceDevant = true;
                        }
                    }
                });

                if (element.piece == null) {
                    if (Y == 1 && !boolPieceDevant) {
                        listPossibilite.push(new Position(X, Y + 2));
                    }
                }
            }

            //Si prend une pièce
            if (element.repX == X + 1 && element.repY == Y + 1) {
                if (element.piece != null) {
                    if (element.piece.color != "Blanc") {
                        listPossibilite.push(new Position(X + 1, Y + 1));
                    }
                }
            }

            if (element.repX == X - 1 && element.repY == Y + 1) {
                if (element.piece != null) {
                    if (element.piece.color != "Blanc") {
                        listPossibilite.push(new Position(X - 1, Y + 1));
                    }
                }
            }
            //
        }
    });
}

function montrerF(color, X, Y) { //Définis les possibilités de mouvement du fou

    var bloqued = false;

    var baseX = X;
    var baseY = Y;
    while (baseX > 0 && baseY > 0 && !bloqued) {
        baseX--;
        baseY--;
        listCase.forEach(element => {
            if (element.repX == baseX && element.repY == baseY) {
                if (element.piece == null) {
                    listPossibilite.push(new Position(baseX, baseY));
                } else {
                    if (element.piece.color != color) {
                        listPossibilite.push(new Position(baseX, baseY));
                    }
                    bloqued = true;
                }
            }
        });
    }

    bloqued = false;
    baseX = X;
    baseY = Y;
    while (baseX < 8 && baseY < 8 && !bloqued) {
        baseX++;
        baseY++;
        listCase.forEach(element => {
            if (element.repX == baseX && element.repY == baseY) {
                if (element.piece == null) {
                    listPossibilite.push(new Position(baseX, baseY));
                } else {
                    if (element.piece.color != color) {
                        listPossibilite.push(new Position(baseX, baseY));
                    }
                    bloqued = true;
                }
            }
        });
    }

    bloqued = false;
    baseX = X;
    baseY = Y;
    while (baseX < 8 && baseY > 0 && !bloqued) {
        baseX++;
        baseY--;
        listCase.forEach(element => {
            if (element.repX == baseX && element.repY == baseY) {
                if (element.piece == null) {
                    listPossibilite.push(new Position(baseX, baseY));
                } else {
                    if (element.piece.color != color) {
                        listPossibilite.push(new Position(baseX, baseY));
                    }
                    bloqued = true;
                }
            }
        });
    }

    bloqued = false;
    baseX = X;
    baseY = Y;
    while (baseX > 0 && baseY < 8 && !bloqued) {
        baseX--;
        baseY++;
        listCase.forEach(element => {
            if (element.repX == baseX && element.repY == baseY) {
                if (element.piece == null) {
                    listPossibilite.push(new Position(baseX, baseY));
                } else {
                    if (element.piece.color != color) {
                        listPossibilite.push(new Position(baseX, baseY));
                    }
                    bloqued = true;
                }
            }
        });
    }

}

function deplacerPion(position) { //Modifie la position de la pièce sélectionnée
    //Initialisation des variables utiles à cette fonction
    var valide = false; //Vérifie si le déplacement est dans la liste des possibilités
    var caseCible = null; //Va recevoir la valeur de la case dans laquelle faire le déplacement
    //

    //On parcourt la liste des possibilités auapravant établie avec les X et Y de la position en pramaètre de la fonction
    listPossibilite.forEach(element => {
        if (element.X == position.X && element.Y == position.Y) { //Si ce la correspond, alors
            valide = true; //Le déplacement est valide
        }
    });
    //Si la position ne correspond à aucune valeur de listPossibilite, 'valide' reste sur la valeur FALSE 

    if (valide) { //Si la variable 'valide' est TRUE
        listCase.forEach(element => { //On parcours la liste des possibilités
            if (element.repX == position.X && element.repY == position.Y) {
                caseCible = element; //On récupère la case concernée
            }
        });
    }

    if (caseCible == null) { //Si la caseCible est vide
        afficherMsg("Déplacement invalide");
        listPossibilite = []; //On réinitialise la liste des possibilités
        deplacementDone = false; //Le déplacement n'a pas été fait -> Pas de changement de tour
    } else {
        if (caseCible.piece != null) {
            if (caseCible.piece.color == "Noir" && playersTurn == 0 || caseCible.piece.color == "Blanc" && playersTurn == 1) {
                listPriseVal.push(caseCible.piece);
                afficherListePrise(); //Refresh la liste des pièces prises
            }
        }
        //Si le pion arrive au bout, devient une dame
        if (caseBase.piece != null) {
            if (caseBase.piece.type == "P") {
                if (caseCible.repY == 7 && caseBase.piece.color == "Blanc" || caseCible.repY == 0 && caseBase.piece.color == "Noir") {
                    caseBase.piece.type = "D";
                }
            }
        }
        //
        caseCible.piece = caseBase.piece; //Modifier la pièce dans la case d'arrivée
        caseBase.piece = null; //Supprimer la pièce dans la case source
        refreshTab(); //Function qui rafait l'affichage de l'échiquier
        deplacementDone = true; //Confirmation de déplacement
        listPossibilite = []; //On vide la liste des possbilités
    }
    masquerListPossib(); //On masque les possibilités sur l'échiquier
}

function affListPossib() { //Affiche les cases vertes de la proposistion de mouvement

    //Parcourt la liste des possibilités 
    listPossibilite.forEach(elemPoss => {
        listCase.forEach(elemCase => {
            if (elemCase.repX == elemPoss.X && elemCase.repY == elemPoss.Y) {
                //elemCase.btn.className = "active";
                var classes = elemCase.btn.classList.value; //Récupération des classes du bouton concerné
                if (elemCase.btn.className.includes("black")) { //Si le nom de la classe comporte le mot "black" -> Case noir
                    classes = classes.replace("black", "active"); //Remplace "black" par "active" -> active = case verte
                    listCaseModif.push(new CaseModif(elemCase.btn, "black")); //Ajout à la liste des modifications
                } else {
                    classes = classes.replace("white", "active"); //Si le nom de la classe comporte le mot "white" -> Case blanche
                    listCaseModif.push(new CaseModif(elemCase.btn, "white")); //Remplace "white" par "active" -> active = case verte
                }
                elemCase.btn.className = classes; //La case passe en vert
            }
        });
    });
}

function masquerListPossib() { //Masque les cases vertes de la proposistion de mouvement

    //On reprend la liste "listCaseModif" pour remettre les cases dans leur couleur d'origine
    listCaseModif.forEach(element => {
        var classes = element.btn.className;
        if (element.color == "black") { //Check la couleur enregistrée
            classes = classes.replace("active", "black"); //Remplace le vert de "active" par "black"
        } else {
            classes = classes.replace("active", "white"); //Remplace le vert de "active" par "white"
        }
        element.btn.className = classes; //On effectue le changement de couleur
    });
    listCaseModif = []; //Vide la liste des cases modifiées
}

function checkIfEchec() { //Check si il y a Echec
    if (echecRoiBlanc) {
        console.log("Echec au roi BLANC");
    }
    if (echecRoiNoir) {
        console.log("Echec au roi NOIR");
    }
}

function checkIfKing(position, colorPlayer) { //Check si la pièce est un roi

    //Vérification de si la pièce
    listCase.forEach(element => {
        if (element.repX == position.X && element.repY == position.Y) {
            if (element.piece != null) {
                if (element.piece.type == "R" && element.piece.color != colorPlayer) {
                    console.log("Est un roi");
                    if (colorPlayer == "Blanc") {
                        console.log("ECHEC !");
                        echecRoiNoir = true;
                    } else {
                        console.log("ECHEC !");
                        echecRoiBlanc = true;
                    }
                }
            }
        }
    });
}

function checkGameOver() { //Si manque un roi
    //Si un des rois est manquant
    victoireBlanc = true;
    victoireNoir = true;
    //Parcours la liste des case pour vérifier si les 2 rois sont toujours présents
    listCase.forEach(element => {
        if (element.piece != null) {
            if (element.piece.type == "R") {
                if (element.piece.color == "Blanc") {
                    victoireNoir = false;
                }
                if (element.piece.color == "Noir") {
                    victoireBlanc = false;
                }
            }
        }
    });
    //

    //Si un des roi est manquant, affiche quel joueur a gagné
    if (victoireBlanc) {
        afficherMsg("Victoire des blancs");
    }
    if (victoireNoir) {
        afficherMsg("Victoire des noirs");
    }
    //
}

function clearPieces() { //Vide l'échiquier

    //Parcourt la liste des cases de l'échiquier et supprime les pièces s'y trouvant
    listCase.forEach(element => {
        if (element.piece != null) {
            element.piece = null;
        }
    });
    //
}

function afficherMsg(message) { //Affiche l'element msgBox ainsi que le message envoyé à l'utilisateur
    //affichage msg
    msgBox.innerHTML = message;
    msgBox.className = "msgBox";
    //
    //Attente
    setTimeout(masquerMsg, 3000);
    //
}

function masquerMsg() { //Masque l'element msgBox
    msgBox.className = "hidden";
}

function ajouterScore(color) {
    if (color == "black") {
        scoreBlack++;
    } else {
        scoreWhite++;
    }
    afficherScores();
    restart();
}

function afficherScores() {
    affScores.innerHTML = "Blancs " + scoreWhite + " - " + scoreBlack + " Noirs";
}

function afficherListePrise() {
    listPrise.innerHTML = "";

    listPriseVal.forEach(element => {
        var elem = document.createElement("img");
        elem.setAttribute("src", "pieces/" + element.type + element.color + ".gif");
        listPrise.appendChild(elem);
    });
}


//OnLoad -> Ce qui suit se lance lors du chargement de la page
onLoad();
//